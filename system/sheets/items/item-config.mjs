import { SYSTEM_ID } from "../../CONSTANTS.mjs";
import GDW_SheetMixin from "../sheet.mjs";

export default class GDW_ItemSheet
  extends GDW_SheetMixin(ItemSheet)
{

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: [SYSTEM_ID, "sheet", "item"],
      tabs: [{
        navSelector: ".tabs",
        contentSelector: ".sheet-body",
        initial: "description"
      }],
      height: 400,
      width: 560
    });
  }

  /* -------------------------------------------- */

  /** @override */
  get template() {
    return `systems/${SYSTEM_ID}/system/templates/items/${this.object.type}-config.hbs`;
  }

  /* -------------------------------------------- */

  /** @override */
  async getData(options) {
    CONFIG.SYSTEM.LOG.trace("GDW_ItemSheet.getData() >", this);

    const context = await super.getData(options);
    const item = context.item;
    const o = item.system;

    if (!o.cost) o.cost = "";
    if (!o.mass) o.mass = "";

    foundry.utils.mergeObject(context, {
      // Item Type, Status, and Details
      is_container: item.system.hasOwnProperty("is_container"),
      is_object: item.system.hasOwnProperty("is_object"),
      item_type: item.type.titleCase(),
      item_status: this._getItemStatus()
      // Item_type: game.i18n.localize(`ITEM.Type${item.type.titleCase()}`),
    });

    CONFIG.SYSTEM.LOG.trace("GDW_ItemSheet.getData() =", context);

    return context;
  }

  /* -------------------------------------------- */

  /**
   * Get the text item status which is shown beneath the Item type in the top-right corner of the sheet.
   * @returns {string|null}  Item status string if applicable to item's type.
   * @private
   */
  _getItemStatus() {
    switch (this.item.type) {
      case "ammunition":
        return game.i18n.localize("Consumable");
      case "magazine":
        return game.i18n.localize("Container");
      case "equipment":
      case "firearm":
      case "weapon":
        return game.i18n.localize(this.item.system.equipped ? "Equipped" : "Unequipped");
    }
  }
}
