import {SYSTEM_ID} from "../../CONSTANTS.mjs";
import GDW_ItemSheet from "./item-config.mjs";

export default class GDW_AmmunitionSheet
  extends GDW_ItemSheet
{

  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: [SYSTEM_ID, "sheet", "item", "ammuniton"],
      width: 500,
      height: 650
    });
  }

  /* -------------------------------------------- */

  /** @override */
  async getData() {
    CONFIG.SYSTEM.LOG.trace("GDW_AmmunitionSheet.getData() >", this);

    const context = await super.getData();

    foundry.utils.mergeObject(context, {
      AMMUNITION_CASE_TYPES: Object.entries(
        CONFIG.SYSTEM.AMMUNITION_CASE_TYPES
      ).reduce((obj, [k, v]) => {
        obj[k] = game.i18n.localize(v.label);
        return obj;
      }, {}),

      AMMUNITION_RIM_TYPES: Object.entries(
        CONFIG.SYSTEM.AMMUNITION_RIM_TYPES
      ).reduce((obj, [k, v]) => {
        obj[k] = game.i18n.localize(v.label);
        return obj;
      }, {})
    });

    CONFIG.SYSTEM.LOG.trace("GDW_AmmunitionSheet.getData() =", context);
    return context;
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);
  }
}
