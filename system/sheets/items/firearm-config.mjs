import {SYSTEM_ID} from "../../CONSTANTS.mjs";
import GDW_ItemDocumentSheet from "./item-config.mjs";

export default class GDW_FirearmConfig
  extends GDW_ItemDocumentSheet
{
  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: [SYSTEM_ID, "sheet", "item", "magazine"],
      width: 725,
      height: 650
    });
  }

  /* -------------------------------------------- */

  /** @override */
  async getData(options={}) {
    CONFIG.SYSTEM.LOG.trace("GDW_FirearmConfig.getData() >", this);

    const context = await super.getData(options);
    const o = context.item.system;

    o.ammo_type = o.ammo_type ? o.ammo_type : "";
    o.barrel_class = o.barrel_class ? o.barrel_class : "";
    o.barrel_type = o.barrel_type ? o.barrel_type : "";
    o.tech_level = o.tech_level ? o.tech_level : "";

    foundry.utils.mergeObject(context, {
      AMMUNITION_TYPES: Object.fromEntries(
        Array.from(game.items.keys()).map(
          key => {
            const item = game.items.get(key);
            if (item.type === "ammunition") return [key, item.name];
            return null;
          }
        ).filter(type => type !== null)
      ),
      BARREL_CLASS: Object.entries(
        CONFIG.SYSTEM.BARREL_CLASS
      ).reduce((obj, [k, v]) => {
        obj[k] = game.i18n.localize(v.label);
        return obj;
      }, {}),
      BARREL_TYPES: Object.entries(
        CONFIG.SYSTEM.BARREL_TYPES
      ).reduce((obj, [k, v]) => {
        obj[k] = game.i18n.localize(v.label);
        return obj;
      }, {})
    });

    CONFIG.SYSTEM.LOG.trace("GDW_FirearmConfig.getData() =", context, this);
    return context;
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);
  }
}
