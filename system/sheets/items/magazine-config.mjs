import {SYSTEM_ID} from "../../CONSTANTS.mjs";
import GDW_ItemDocumentSheet from "./item-config.mjs";

export default class GDW_MagazineConfig
  extends GDW_ItemDocumentSheet
{
  /** @override */
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: [SYSTEM_ID, "sheet", "item", "magazine"],
      width: 725,
      height: 650
    });
  }

  /* -------------------------------------------- */

  /** @override */
  async getData(options={}) {
    CONFIG.SYSTEM.LOG.trace("GDW_MagazineConfig.getData() >", this);

    const context = await super.getData(options);

    CONFIG.SYSTEM.LOG.debug("super context: ", context);
    CONFIG.SYSTEM.LOG.debug("game.items: ", game.items);

    foundry.utils.mergeObject(context, {
      AMMUNITION_TYPES: Object.fromEntries(
        Array.from(game.items.keys()).map(
          key => {
            const item = game.items.get(key);
            if (item.type === "ammunition") return [key, item.system.metric_name];
            return null;
          }
        ).filter(type => type !== null)
      ),

      COMPATIBILITY: Object.fromEntries(
        Array.from(game.items.keys()).map(
          key => {
            const item = game.items.get(key);
            if (item.type === "firearm") return [key, item.system.name];
            return null;
          }
        ).filter(type => type !== null)
      )
    });

    CONFIG.SYSTEM.LOG.trace("GDW_MagazineConfig.getData() =", context);
    return context;
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);
  }
}
