// Import Decimal from "decimal.js";

export default class GDW_Item extends Item {
  prepareDerivedData() {
    super.prepareDerivedData();
    switch ( this.type ) {
      case "ammunition": return this._prepareAmmunitionDerivedData();
      case "magazine": return this._prepareMagazineDerivedData();
      case "firearm": return this._prepareFirearmDerivedData();
    }
  }

  /* --------  Firearm Weapon -------- */

  _prepareFirearmDerivedData() {
    CONFIG.SYSTEM.LOG.trace("GDW_Item._prepareFirearmDerivedData() >", this);

    const o = this.system;

    // -- Barrel Mass --
    if (o.barrel_class && o.actual_barrel_length) {
      this.barrel_mass =
        CONFIG.SYSTEM.BARREL_CLASS[o.barrel_class].factor
        * o.actual_barrel_length;
    }

    // -- Barrel Cost --
    if (this.barrel_mass && o.barrel_type) {
      this.barrel_cost =
        CONFIG.SYSTEM.BARREL_TYPES[o.barrel_type].cost_modifier[o.barrel_class]
        * this.barrel_mass;
    }

    // -- Average Barrel Length --
    let entries = [];
    if (o.ammo_type && o.barrel_type) {
      const ammo = game.items.get(o.ammo_type).system;

      const avg_muzzle_energy = this._getAvgMuzzleEnergy(ammo);
      const rifling_multiplier =
        CONFIG.SYSTEM.BARREL_TYPES[o.barrel_type].rifling_multiplier;

      if (o.barrel_type.length > 0 && ammo.tech_level.length > 0) {
        const tl_match = ammo.tech_level.match(/^(\d+)(?:-(\d+))?$/);
        const tl_start = tl_match[1];
        const tl_finish = tl_match[2] == null ? tl_start : tl_match[2];
        for (let tl = tl_start; tl <= tl_finish; tl++) {
          if (!avg_muzzle_energy.hasOwnProperty(tl)) continue;

          const avg_barrel_length =
            avg_muzzle_energy[tl] / (ammo.caliber ** 2) * rifling_multiplier;
          CONFIG.SYSTEM.LOG.debug(`avg_barrel_length: (${o.barrel_type}) ${tl}: ${avg_muzzle_energy[tl]} / ${ammo.caliber ** 2} * ${rifling_multiplier} = ${avg_barrel_length}`);

          const act_muzzle_energy =
            avg_muzzle_energy[tl] / 2 * (1 + (o.actual_barrel_length / avg_barrel_length));
          CONFIG.SYSTEM.LOG.debug(`act_muzzle_energy: ${tl}: ${avg_muzzle_energy[tl]} / 2 * (1 + ${o.actual_barrel_length} / ${avg_barrel_length}) = ${act_muzzle_energy}`);

          const dmg = Math.sqrt(act_muzzle_energy) / 15;
          const dice = Math.floor(dmg);
          const frac = dice > 0 ? dmg - dice : -dmg;
          const mod =
            frac > (2 / 3) ? "+2"
              : frac > (1 / 3) ? "+1"
                : frac > 0 ? ""
                  : frac > (-1 / 3) ? "-3"
                    : frac > (-2 / 3) ? "-2"
                      : "-1";
          const damage_value = `${dice > 0 ? dice : 1}D${mod}`;
          CONFIG.SYSTEM.LOG.debug(`damage_value: ${tl}: sqrt(${act_muzzle_energy}) / 15 = ${damage_value}`);

          const penetration =
            act_muzzle_energy > 50000 ? "2-2-2"
              : act_muzzle_energy > 20000 ? "2-2-3"
                : act_muzzle_energy > 10000 ? "2-2-4"
                  : act_muzzle_energy > 5000 ? "2-2-6"
                    : act_muzzle_energy > 3000 ? "2-3-Nil"
                      : act_muzzle_energy > 2000 ? "2-Nil"
                        : act_muzzle_energy > 600 ? "1-Nil"
                          : "Nil";
          CONFIG.SYSTEM.LOG.debug(`penetration: ${tl}: ${act_muzzle_energy} -> ${penetration}`);

          entries.push([tl, {
            avg_barrel_length: avg_barrel_length,
            act_muzzle_energy: act_muzzle_energy,
            damage_value: damage_value,
            penetration: penetration
          }]);
        }
      }
    }
    this.barrel = Object.fromEntries(entries);

    CONFIG.SYSTEM.LOG.trace("GDW_Item._prepareFirearmDerivedData() <", this);
  }

  /* --------  Ammunition -------- */

  _getAvgMuzzleEnergy(ammo) {
    CONFIG.SYSTEM.LOG.trace("GDW_Item._getAvgMuzzleEnergy() >", this, ammo);
    const case_type = CONFIG.SYSTEM.AMMUNITION_CASE_TYPES[ammo.case_type];

    let entries = [];
    if (ammo.tech_level.length > 0) {
      const tl_match = ammo.tech_level.match(/^(\d+)(?:-(\d+))?$/);
      const tl_start = tl_match[1];
      const tl_finish = tl_match[2] == null ? tl_start : tl_match[2];
      for (let tl = tl_start; tl <= tl_finish; tl++) {
        const tl_mod = 0.6 + (tl >= 9 ? 0.7 : 0.1 * (tl - 3));
        const me_mod = case_type.muzzle_energy_mod[ammo.electrothermal_chemical ? 1 : 0];
        const avg_muzzle_energy =
          tl_mod * me_mod * ammo.case_length * Math.PI * (ammo.caliber ** 2) * 0.25;
        CONFIG.SYSTEM.LOG.debug(`avg_muzzle_energy: ${tl}: ${tl_mod} * ${me_mod} * ${ammo.case_length} * ${Math.PI} * ${ammo.caliber ** 2} * 0.25 = `, avg_muzzle_energy);
        entries.push([tl, avg_muzzle_energy]);
      }
    }

    const result = Object.fromEntries(entries);
    CONFIG.SYSTEM.LOG.trace("GDW_Item._getAvgMuzzleEnergy() <", ammo, result);
    return result;
  }

  _prepareAmmunitionDerivedData() {
    CONFIG.SYSTEM.LOG.trace("GDW_Item._prepareAmmunitionDerivedData() >", this);
    const o = this.system;

    const case_type = CONFIG.SYSTEM.AMMUNITION_CASE_TYPES[o.case_type];
    const rim_type = CONFIG.SYSTEM.AMMUNITION_RIM_TYPES[o.rim_type];

    this.metric_name =
      o.caliber > 0 && o.case_length > 0
        ? `${o.caliber}×${o.case_length}mm${rim_type.abbrev}`
        : "";
    CONFIG.SYSTEM.LOG.debug("metric_name: ", this.metric_name);

    this.cartridge_length =
      o.case_length + (case_type.length_factor * o.caliber);
    CONFIG.SYSTEM.LOG.debug(`cartridge_length: ${o.case_length} + ${case_type.length_factor} * ${o.caliber} = ${this.cartridge_length}`, case_type);

    this.cartridge_mass =
      case_type.density * o.case_length * Math.PI * (o.caliber ** 2) * 0.25;
    CONFIG.SYSTEM.LOG.debug(`cartridge_mass: ${case_type.density} * ${o.case_length} * ${Math.PI} * ${o.caliber ** 2} * 0.25 = ${this.cartridge_mass}`, case_type);

    this.mass = this.cartridge_mass * 0.001;

    const avg_muzzle_energy = this.avg_muzzle_energy =
      this._getAvgMuzzleEnergy(o);
    CONFIG.SYSTEM.LOG.debug("avg_muzzle_energy: ", avg_muzzle_energy);

    CONFIG.SYSTEM.LOG.trace("GDW_Item._prepareAmmunitionDerivedData() <");
  }

  /* --------  Magazine -------- */

  _prepareMagazineDerivedData() {
    CONFIG.SYSTEM.LOG.trace("GDW_Item._prepareMagazineDerivedData() >", this);
    const o = this.system;
    const rounds = o.capacity.max - o.capacity.value;

    this.empties = [...Array(o.capacity.value).keys()];
    this.rounds = [...Array(rounds).keys()];

    const item = game.items.get(o.ammo_type);
    if (item) {
      this.mass = o.container_mass + (Number(rounds) * item.system.cartridge_mass / 1000);
    }

    CONFIG.SYSTEM.LOG.trace("GDW_Item._prepareMagazineDerivedData() <");
  }
}
