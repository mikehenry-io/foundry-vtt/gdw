import {SYSTEM_ID, SYSTEM_LOG_LEVELS} from "../CONSTANTS.mjs";
import {init} from "./init.mjs";
// Import {renderSettings} from "./renderSettings.mjs";
// import {itemCreate, itemUpdate, itemDelete} from "./itemCRUD.mjs";

Hooks.once("init", init);
Hooks.once("devModeReady", ({ registerPackageDebugFlag }) => {
  registerPackageDebugFlag(SYSTEM_ID, "level", {
    choiceLabelOverrides: SYSTEM_LOG_LEVELS
  });
});
// Hooks.on("renderSettings", renderSettings);
// Hooks.on("createItem", itemCreate);
// Hooks.on("updateItem", itemUpdate);
// Hooks.on("deleteItem", itemDelete);
