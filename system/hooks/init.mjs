import {SYSTEM, SYSTEM_ID, SYSTEM_NAME, SYSTEM_LOGO} from "../CONSTANTS.mjs";
// Import GDW_DetailedCharacterDataModel from "../datamodels/actor/detailed-character.mjs";
// import GDW_TemplateCharacterDataModel from "../datamodels/actor/template-character.mjs";
import GDW_AmmunitionDataModel from "../datamodels/item/ammunition.mjs";
import GDW_FirearmDataModel from "../datamodels/item/firearm.mjs";
import GDW_MagazineDataModel from "../datamodels/item/magazine.mjs";
// Import EquipmentDataModel from "../datamodels/item/equipment.mjs";
// import HeritageDataModel from "../datamodels/item/heritage.mjs";
// import LineageDataModel from "../datamodels/item/lineage.mjs";
// import TalentDataModel from "../datamodels/item/talent.mjs";
import GDW_Actor from "../documents/actor.mjs";
import GDW_AmmunitionSheet from "../sheets/items/ammunition-config.mjs";
import GDW_FirearmConfig from "../sheets/items/firearm-config.mjs";
import GDW_MagazineConfig from "../sheets/items/magazine-config.mjs";
// Import BackgroundConfig from "../sheets/items/background-config.mjs";
// import HeritageConfig from "../sheets/items/heritage-config.mjs";
// import LineageConfig from "../sheets/items/lineage-config.mjs";
// import PcConfig from "../sheets/actors/pc-config.mjs";
import GDW_Item from "../documents/item.mjs";
// Import ClassConfig from "../sheets/items/class-config.mjs";
// import ClassDataModel from "../datamodels/item/class.mjs";
import * as utils from "../utils/_module.mjs";

/**
 *
 */
export function init() {
  console.log(
    `${SYSTEM_ID} | Initializing the ${SYSTEM_NAME} Game System`,
    SYSTEM_LOGO.join("\n")
  );

  CONFIG.SYSTEM = SYSTEM;

  registerDataModels();
  registerDocumentSheets();
  registerDocumentClasses();
  registerHandlebarsHelpers();

  utils.preloadHandlebarsTemplates();
}

/* -------------------------------------------- */

/**
 *
 */
function registerDataModels() {
  CONFIG.Actor.dataModels = {
    // Detailed_character: GDW_DetailedCharacterDataModel,
    // template_character: GDW_TemplateCharacterDataModel
  };

  CONFIG.Item.dataModels = {
    ammunition: GDW_AmmunitionDataModel,
    firearm: GDW_FirearmDataModel,
    magazine: GDW_MagazineDataModel
  };
}

/* -------------------------------------------- */

/**
 *
 */
function registerDocumentClasses() {
  CONFIG.Actor.documentClass = GDW_Actor;
  CONFIG.Item.documentClass = GDW_Item;
}

/* -------------------------------------------- */

/**
 *
 */
function registerDocumentSheets() {
  if ( CONFIG.SYSTEM.debugLevel() > 0 ) {
    Actors.unregisterSheet("core", ActorSheet);
    Items.unregisterSheet("core", ItemSheet);
  }

  // // Actors
  // Actors.registerSheet(SYSTEM_ID, PcConfig, {types: ["pc"], makeDefault: true});

  // Items
  Items.registerSheet(SYSTEM_ID, GDW_AmmunitionSheet, {
    types: ["ammunition"],
    makeDefault: true,
    label: () => game.i18n.localize("Ammunition")
  });
  Items.registerSheet(SYSTEM_ID, GDW_MagazineConfig, {
    types: ["magazine"],
    makeDefault: true,
    label: () => game.i18n.localize("Magazine")
  });
  Items.registerSheet(SYSTEM_ID, GDW_FirearmConfig, {
    types: ["firearm"],
    makeDefault: true,
    label: () => game.i18n.localize("Firearm")
  });
}

/* -------------------------------------------- */

/**
 *
 */
function registerHandlebarsHelpers() {

  // Make the first letter uppercase and the rest lower
  Handlebars.registerHelper("capitalize", function(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  });

  // Convert a number to base 34 notation
  Handlebars.registerHelper("fixed", function(number, decimals=0) {
    return parseFloat(number).toFixed(decimals);
  });

  // Convert a number to base 34 notation
  Handlebars.registerHelper("precision", function(number, precision) {
    return parseFloat(number).toPrecision(precision);
  });

  // Convert a number to base 34 notation
  Handlebars.registerHelper("tetratrigesimal", function(decimal) {
    return CONFIG.SYSTEM.TETRATRIGESIMAL[parseInt(decimal)];
  });

  // Truncate a string to a certain length with an ellipsis
  Handlebars.registerHelper("truncate", (str, len) => {
    if (str.length > len) {
      return `${str.slice(0, len)}...`;
    }
    return str;
  });

  // Convert a type and value to a localized label
  Handlebars.registerHelper("typeLabel", (type, value) => {
    return game.i18n.localize(CONFIG.SYSTEM[type][value]?.label);
  });
}
