import GDW_AmmunitionDataModel from "./ammunition.mjs";
import GDW_MagazineDataModel from "./magazine.mjs";

export {
  GDW_AmmunitionDataModel,
  GDW_MagazineDataModel
};
export { default as GDW_ItemMetadataTemplate } from "./templates/metadata.mjs";

export const config = {
  ammunition: GDW_AmmunitionDataModel,
  magazine: GDW_MagazineDataModel
};
