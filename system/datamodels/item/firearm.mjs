import GDW_SystemDataModel from "../system.mjs";
import GDW_ItemMetadataTemplate from "./templates/metadata.mjs";
import GDW_ItemObjectTemplate from "./templates/object.mjs";

export default class GDW_FirearmDataModel
  extends GDW_SystemDataModel.mixin(
    GDW_ItemMetadataTemplate, GDW_ItemObjectTemplate
  )
{
  /** @inheritDoc */
  static defineSchema() {
    const fields = foundry.data.fields;
    return this.mergeSchema(super.defineSchema(), {
      actual_barrel_length: new fields.NumberField({
        min: 0, step: 0.1, label: "Actual Barrel Length"
      }),
      ammo_type: new fields.StringField({
        required: false, label: "Ammo Type"
      }),
      barrel_class: new fields.StringField({
        required: false, label: "Barrel Weight Class",
        choices: CONFIG.SYSTEM.BARREL_CLASS
      }),
      barrel_type: new fields.StringField({
        required: false, label: "Barrel Type",
        choices: CONFIG.SYSTEM.BARREL_TYPES
      }),
      magazine: new fields.BooleanField({
        required: false, label: "Magazine or single shot loaded"
      })
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  static migrateData(source) {
    super.migrateData(source);
  }
}
