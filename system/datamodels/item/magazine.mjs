import GDW_SystemDataModel from "../system.mjs";
import GDW_ItemContainerTemplate from "./templates/container.mjs";
import GDW_ItemMetadataTemplate from "./templates/metadata.mjs";
import GDW_ItemObjectTemplate from "./templates/object.mjs";
import GDW_Item from "../../documents/item.mjs";

export default class GDW_MagazineDataModel
  extends GDW_SystemDataModel.mixin(
    GDW_ItemMetadataTemplate, GDW_ItemObjectTemplate, GDW_ItemContainerTemplate
  )
{
  /** @inheritDoc */
  static defineSchema() {
    const fields = foundry.data.fields;
    return this.mergeSchema(super.defineSchema(), {
      ammo_type: new fields.StringField({
        required: false, initial: "", label: "Ammo Type"
      }),
      compatibility: new fields.SetField(new fields.StringField({
        required: false, initial: [], label: "Compatibility"
      }))
    });
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  static migrateData(source) {
    super.migrateData(source);
  }
}
