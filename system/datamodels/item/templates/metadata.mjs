/**
 * Data model template with item description & source.
 *
 * @property {object} description               Various item descriptions.
 * @property {string} description.value         Full item description.
 * @property {string} description.chat          Description displayed in chat card.
 * @property {string} description.unidentified  Description displayed if item is unidentified.
 * @property {string} source                    Adventure or sourcebook where this item originated.
 * @mixin
 */
export default class GDW_ItemMetadataTemplate
  extends foundry.abstract.DataModel
{
  /** @inheritdoc */
  static defineSchema() {
    const fields = foundry.data.fields;
    return {
      description: new fields.SchemaField({
        value: new fields.HTMLField({required: true, nullable: true, label: "Description"}),
        chat: new fields.HTMLField({required: true, nullable: true, label: "Chat Description"}),
        unidentified: new fields.HTMLField({
          required: true, nullable: true, label: "Unidentified Description"
        })
      }),
      source: new foundry.data.fields.StringField({required: true, label: "Source"})
    };
  }

  // /** @inheritDoc */
  // static defineSchema() {
  //   const fields = foundry.data.fields;
  //   return {
  //     description: new fields.HTMLField({required: true, nullable: true, label: "Description"}),
  //     tech_level: new fields.NumberField({min: 0, initial: 0, integer: true})
  //   };
  // }

  /* -------------------------------------------- */

  /** @inheritdoc */
  static migrateData(source) {
    GDW_ItemMetadataTemplate.#migrateSource(source);
  }

  /* -------------------------------------------- */

  /**
   * Convert null source to the blank string.
   * @param {object} source  The candidate source data from which the model will be constructed.
   */
  static #migrateSource(source) {
    if ( source.source === null ) source.source = "";
  }
}
