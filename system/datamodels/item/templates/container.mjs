export default class GDW_ItemContainerTemplate
  extends foundry.abstract.DataModel
{
  /** @inheritdoc */
  static defineSchema() {
    const fields = foundry.data.fields;
    return {
      is_container: new fields.SchemaField({}),
      capacity: new fields.SchemaField({
        value: new fields.NumberField({ initial: 0, min: 0, label: "Current" }),
        max: new fields.NumberField({ min: 0, label: "Limit" }),
        type: new fields.StringField({
          choices: CONFIG.SYSTEM.CONTAINER_CAPACITY_TYPES, initial: "unit",
          required: true
        })
      }),
      container_mass: new fields.NumberField({ initial: 0, min: 0, label: "Container Mass" })
    };
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  static migrateData(source) {
    if (source.capacity.value === null) source.capacity.value = 0;
    if (!Object.keys(CONFIG.SYSTEM.CONTAINER_CAPACITY_TYPES).includes(source.capacity.type)) source.capacity.type = "unit";
  }

  /* -------------------------------------------- */

  /**
   * Convert null source to the blank string.
   * @param {object} source  The candidate source data from which the model will be constructed.
   */
  // static #migrateSource(source) {
  //   if ( source.source === null ) source.source = "";
  // }
}
