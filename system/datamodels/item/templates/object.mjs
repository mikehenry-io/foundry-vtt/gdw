export default class GDW_ItemObjectTemplate
  extends foundry.abstract.DataModel
{
  /** @inheritdoc */
  static defineSchema() {
    const fields = foundry.data.fields;
    return {
      is_object: new fields.SchemaField({}),
      cost: new fields.NumberField({ label: "Cost" }),
      mass: new fields.NumberField({label: "Mass"}),
      tech_level: new fields.StringField({
        min: "0", label: "Tech Level",
        validate: v => /^\d+(?:-\d+)?$/.test(v)
      })
    };
  }

  /* -------------------------------------------- */

  /** @inheritdoc */
  static migrateData(source) {
    // ItemDescriptionTemplate.#migrateSource(source);
  }

  /* -------------------------------------------- */

  /**
   * Convert null source to the blank string.
   * @param {object} source  The candidate source data from which the model will be constructed.
   */
  // static #migrateSource(source) {
  //   if ( source.source === null ) source.source = "";
  // }
}
