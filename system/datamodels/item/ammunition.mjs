import GDW_SystemDataModel from "../system.mjs";
import GDW_ItemMetadataTemplate from "./templates/metadata.mjs";
import GDW_ItemObjectTemplate from "./templates/object.mjs";

export default class GDW_AmmunitionDataModel
  extends GDW_SystemDataModel.mixin(
    GDW_ItemMetadataTemplate, GDW_ItemObjectTemplate
  )
{
  /** @inheritDoc */
  static defineSchema() {
    const fields = foundry.data.fields;
    return this.mergeSchema(super.defineSchema(), {
      caliber: new fields.NumberField({
        min: 0, step: 0.01, label: "Caliber"
      }),
      case_length: new fields.NumberField({
        min: 0, step: 0.1, label: "Case Length"
      }),
      case_type: new fields.StringField({
        choices: CONFIG.SYSTEM.AMMUNITION_CASE_TYPES, initial: "bottlenecked",
        required: true
      }),
      electrothermal_chemical: new fields.BooleanField({initial: false}),
      rim_type: new fields.StringField({
        choices: CONFIG.SYSTEM.AMMUNITION_RIM_TYPES, initial: "rimless",
        required: true
      })
    });
  }
}
