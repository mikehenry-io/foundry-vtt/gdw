import GDW_Item from "../../documents/item.mjs";

export default class GDW_BaseCharacterDataModel
  extends foundry.abstract.DataModel
{

  /** @inheritDoc */
  static _enableV10Validation = true;

  /** @inheritDoc */
  static defineSchema() {
    const fields = foundry.data.fields;
    return {
    };
  }
}
