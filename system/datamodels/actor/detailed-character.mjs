import GDW_BaseCharacterDataModel from "./base-character.mjs";

export default class GDW_DetailedCharacterDataModel
  extends GDW_BaseCharacterDataModel
{
  /** @inheritDoc */
  static defineSchema() {
    const fields = foundry.data.fields;
    return Object.assign(super.defineSchema(), {
    });
  }
}
