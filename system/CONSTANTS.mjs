export const SYSTEM_ID = "GDW";
export const SYSTEM_NAME = "GDW House Rules";
export const SYSTEM_LOGO = [
  "                                                                   ",
  "                     ╔═══════════════════════╗                     ",
  "                     ║   1 9 7 3 - 1 9 9 6   ║                     ",
  "                     ╠═══════════════════════╣                     ",
  "                     ║  ▄▀▀▀▀ ▒█▀▀▀▄ ▒█  ▒█  ║                     ",
  "                     ║ ░█  ▀▀▀▀█  ▒█ ▒█▒█▒█  ║                     ",
  "                     ║  ▀▄▄▄█ ▒█▄▄▄▀ ▒█▄▀▄█  ║                     ",
  "                     ╠═══════════════════════╣                     ",
  "                     ║     G  A  M  E  S     ║                     ",
  "                     ╚═══════════════════════╝                     ",
  "The Traveller game in all forms is owned by Far Future Enterprises.",
  "            Copyright 1977 - 2008 Far Future Enterprises           "
];

export const SYSTEM_LOG_LEVELS = {
  0: "FATAL",
  1: "ERROR",
  2: "WARN ",
  3: "INFO ",
  4: "DEBUG",
  5: "TRACE"
};

/**
 *
 */
function debugLevel() {
  return game.modules.get("_dev-mode")?.api?.getPackageDebugValue(SYSTEM_ID, "level");
}

/**
 *
 * @param level
 * @param {...any} args
 */
function log(level, ...args) {
  try {
    if (level <= debugLevel()) console.log(`${SYSTEM_ID} | ${SYSTEM_LOG_LEVELS[level]}`, ...args);
  } catch(e) {}
}

const LOG = {
  fatal: (...args) => log(0, ...args),
  error: (...args) => log(1, ...args),
  warn: (...args) => log(2, ...args),
  info: (...args) => log(3, ...args),
  debug: (...args) => log(4, ...args),
  trace: (...args) => log(5, ...args)
};

/**
 * The ammunition casing types.
 * @type {{label: string}}
 */
const AMMUNITION_CASE_TYPES = {
  bottlenecked: {
    label: "Bottlenecked", density: 0.011, length_factor: 2,
    muzzle_energy_mod: [1.6, 3.2]
  },
  shotgun_shell: {
    label: "Shotgun-shell", density: 0.003, length_factor: 0,
    muzzle_energy_mod: [0.2, 0.3]
  },
  straight_walled: {
    label: "Straight-walled", density: 0.008, length_factor: 1,
    muzzle_energy_mod: [0.4, 0.8]
  }
};

/**
 * The ammunition casing types.
 * @type {{label: string}}
 */
const AMMUNITION_RIM_TYPES = {
  rimless: {label: "Rimless", abbrev: "" },
  rimmed: { label: "Rimmed", abbrev: "R" },
  semi_rimmed: { label: "Semi-rimmed", abbrev: "SR" },
  rebated: { label: "Rebated rim", abbrev: "RR" },
  belted: { label: "Belted", abbrev: "B" }
};

/**
 * The container capacity types.
 * @type {{label: string}}
 */
const BARREL_CLASS = {
  light: { label: "Light", factor: 0.002 },
  heavy: { label: "Heavy", factor: 0.003 }
};

/**
 * The container capacity types.
 * @type {{label: string}}
 */
const BARREL_TYPES = {
  smoothbore: {
    label: "Smoothbore",
    rifling_multiplier: 40,
    cost_modifier: { light: 100, heavy: 100 }
  },
  smoothbore_etc: {
    label: "Smoothbore ETC",
    rifling_multiplier: 30,
    cost_modifier: { light: 100, heavy: 100 }
  },
  rifled: {
    label: "Rifled",
    rifling_multiplier: 10,
    cost_modifier: { light: 200, heavy: 400 }
  },
  rifled_etc: {
    label: "Rifled ETC",
    rifling_multiplier: 5,
    cost_modifier: { light: 200, heavy: 400 }
  }
};

/**
 * The container capacity types.
 * @type {{label: string}}
 */
const CONTAINER_CAPACITY_TYPES = {
  unit: { label: "Unit" },
  volume: { label: "Volume" },
  weight: { label: "Weight" }
};

/**
 * An array of tetratrigesimal digits for O[1]/O[n] conversion.
 *
 * @type {string[]}
 */
const TETRATRIGESIMAL = [
  "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
  "a", "b", "c", "d", "e", "f", "g", "h", "j", "k",
  "l", "m", "n", "p", "q", "r", "s", "t", "u", "v",
  "w", "x", "y", "z"
];

/**
 * Include all constant definitions within the SYSTEM global export
 * @type {object}
 */
export const SYSTEM = {
  id: SYSTEM_ID,
  name: SYSTEM_NAME,
  debugLevel,
  AMMUNITION_CASE_TYPES,
  AMMUNITION_RIM_TYPES,
  BARREL_CLASS,
  BARREL_TYPES,
  CONTAINER_CAPACITY_TYPES,
  TETRATRIGESIMAL,
  LOG
};
