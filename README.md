# GDW House Rules

Welcome to this fan-created, unofficial, totally free GDW House Rules Foundry VTT game system implementation. We hope you will enjoy playing it as much as we did implementing it!

Even though this system draws its locus primarily from _Traveller: The New Era_ (TNE) and includes minor tweeks, corrections and upgrades in its adaptation to Foundry VTT, it intends to be compatible with the GDW House Rules system of the 1990's, though it was never published as such, and should be usable out-of-the-box with not only _Traveller: The New Era_ but also Twilight: 2000 v2.2, Dark Conspiracy and other TTRPGs available on CDROM from [Far Future Enterprises](https://farfuture.net).

The Traveller game in all forms is owned by Far Future Enterprises. Copyright 1977 - 2008 Far Future Enterprises.

## FAQ

Q: Why spend precious time implementing a game system that has been out-of-print for nigh on 30 years?

A: Because as the result of over 20 years of game development before GDW's doors were closed, we think the GDW House Rules deserves a Foundry VTT game system and, compared to today's offerings in the TTRPG marketplace, we've found this particular game system very satisfying to play, both in-group and solo, as well as create this kind of software for.
